# Compose to generate odoo and postgres instance using HomebrewSoft's repo and Portainer app

## Deploy HomebrewSoft's repo
- Ubuntu server
- Clone HomebrewSoft's repo: 
```bash
$ git clone https://gitlab.com/HomebrewSoft/homebrewsh/odoo-instances
```
- Deploy repo: 
```bash 
$ cd odoo-instances && sudo sh setup
Odoo Manager Version: 13.0
Domain: my.domain.net                
Contact email: ed.rodriguez97@gmail.com
```
- Push odoo.conf file on MAIN_PATH (Root path where will be all data, by default "/home/ubuntu"):
[odoo.conf](https://gitlab.com/HomebrewSoft/homebrewsh/odoo-instances/-/blob/server/templates/13.0/config/odoo.conf)
```bash
$ cd MAIN_PATH
$ nano odoo.conf
$ chmod u=r,g=r,o=r odoo.conf
```
![alt text](./images/odoo-conf.png)

## Portainer

### Deploy and init
- https://www.portainer.io/installation
```bash
$ sudo docker volume create portainer_data

$ sudo docker run -d -p 8000:8000 -p 9000:9000\
 --name=portainer --restart=always\
 -v /var/run/docker.sock:/var/run/docker.sock\
 -v portainer_data:/data portainer/portainer
```

# Solution 1
Configure template with odoo and postgres deployment:
[ODOO-POSTGRES](./README-DEPLOY-ODOO-POSTGRES.md)

# Solution 2

#### Configure template with only postgres deployment:
[POSTGRES](./README-DEPLOY-POSTGRES.md)

#### Configure template with only postgres deployment:
[ODOO](./README-DEPLOY-ODOO.md)

# Manage instances

Containers menu![alt text](./images/container-menu.png)
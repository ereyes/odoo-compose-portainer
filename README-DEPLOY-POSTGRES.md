# Create and deploy template for postgres on Portainer app

## Create template

### Select server
![alt text](./images/select-server.png)

### Select template menu
![alt text](./images/select-template-menu.png)

### Configure template postgres data
![alt text](./images/configure-postgres-template.png)

The [original docker-compose](https://gitlab.com/HomebrewSoft/homebrewsh/odoo-instances/-/blob/server/templates/13.0/docker-compose.yml) was modified with this [new docker-compose](https://gitlab.com/ereyes/odoo-compose-portainer/-/blob/master/docker-compose.postgres.yml), the main modifications were oriented to the use of environment variables

### Configure enviroments variables
DB_VERSION:![alt text](./images/configure-postgres-template-variable-version.png)

DB_USER:![alt text](./images/configure-postgres-template-variable-dbuser.png)

DB_PASSWORD:![alt text](./images/configure-postgres-template-variable-dbpassword.png)

CREATE:![alt text](./images/create-template.png)

### Variables:
- DB_VERSION: Postgres versions (11.5) by default:"11.5".
- DB_NAME: Main db's name, by default "postgres".
- DB_USER: Main user's name, by defult "odoo".
- DB_PASSWORD: Password "odoo".

## Deploy new instance using template

Select server:![alt text](./images/select-server.png)

Select template menu:![alt text](./images/select-postgres-template.png)

Deploy template:![alt text](./images/deploy-postgres-template.png)

## Manage instances

Containers menu![alt text](./images/container-menu.png)